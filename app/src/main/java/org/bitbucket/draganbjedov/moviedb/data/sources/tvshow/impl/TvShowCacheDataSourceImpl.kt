package org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.impl

import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShow
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.TvShowCacheDataSource
import javax.inject.Inject

class TvShowCacheDataSourceImpl @Inject constructor() : TvShowCacheDataSource {

    private val shows = ArrayList<TvShow>()

    override suspend fun getTvShows(): List<TvShow> = shows

    override suspend fun saveTvShows(shows: List<TvShow>) {
        this.shows.clear()
        this.shows.addAll(shows)
    }
}
