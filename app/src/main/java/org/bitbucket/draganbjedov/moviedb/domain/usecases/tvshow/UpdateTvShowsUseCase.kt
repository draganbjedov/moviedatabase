package org.bitbucket.draganbjedov.moviedb.domain.usecases.tvshow

import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShow
import org.bitbucket.draganbjedov.moviedb.domain.repositories.TvShowRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UpdateTvShowsUseCase @Inject constructor(private val tvShowRepository: TvShowRepository) {

    suspend fun execute(): List<TvShow> = tvShowRepository.updateTvShows()
}
