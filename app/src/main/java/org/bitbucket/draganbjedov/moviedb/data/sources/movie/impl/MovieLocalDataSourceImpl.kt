package org.bitbucket.draganbjedov.moviedb.data.sources.movie.impl

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.bitbucket.draganbjedov.moviedb.data.db.dao.MovieDao
import org.bitbucket.draganbjedov.moviedb.data.model.movie.Movie
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.MovieLocalDataSource
import javax.inject.Inject

class MovieLocalDataSourceImpl @Inject constructor(private val movieDao: MovieDao) : MovieLocalDataSource {

    override suspend fun getMovies(): List<Movie> = movieDao.getAll()

    override suspend fun saveMovies(movies: List<Movie>) {
        CoroutineScope(Dispatchers.IO).launch {
            movieDao.save(movies)
        }
    }

    override suspend fun clear() {
        CoroutineScope(Dispatchers.IO).launch {
            movieDao.deleteAll()
        }
    }
}
