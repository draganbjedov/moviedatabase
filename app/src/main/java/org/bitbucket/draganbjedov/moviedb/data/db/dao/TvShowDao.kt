package org.bitbucket.draganbjedov.moviedb.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShow

@Dao
interface TvShowDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(movies: List<TvShow>)

    @Query("DELETE FROM tv_show")
    suspend fun deleteAll()

    @Query("SELECT * FROM tv_show")
    suspend fun getAll(): List<TvShow>
}
