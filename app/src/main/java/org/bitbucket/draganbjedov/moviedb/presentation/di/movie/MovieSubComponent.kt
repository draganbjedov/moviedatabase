package org.bitbucket.draganbjedov.moviedb.presentation.di.movie

import dagger.Subcomponent
import org.bitbucket.draganbjedov.moviedb.presentation.activities.movie.MoviesActivity

@MovieScope
@Subcomponent(modules = [MovieModule::class])
interface MovieSubComponent {
    fun inject(activity: MoviesActivity)

    @Subcomponent.Factory
    interface Factory {
        fun create(): MovieSubComponent
    }
}
