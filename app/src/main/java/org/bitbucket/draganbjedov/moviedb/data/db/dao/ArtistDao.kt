package org.bitbucket.draganbjedov.moviedb.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.bitbucket.draganbjedov.moviedb.data.model.artist.Artist

@Dao
interface ArtistDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(artists: List<Artist>)

    @Query("DELETE FROM artist")
    suspend fun deleteAll()

    @Query("SELECT * FROM artist")
    suspend fun getAll(): List<Artist>
}
