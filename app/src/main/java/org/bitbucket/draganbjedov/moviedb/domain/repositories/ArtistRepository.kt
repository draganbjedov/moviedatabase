package org.bitbucket.draganbjedov.moviedb.domain.repositories

import org.bitbucket.draganbjedov.moviedb.data.model.artist.Artist

interface ArtistRepository {

    suspend fun getArtists(): List<Artist>
    suspend fun updateArtists(): List<Artist>
}
