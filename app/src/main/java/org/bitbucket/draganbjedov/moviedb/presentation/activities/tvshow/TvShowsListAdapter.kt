package org.bitbucket.draganbjedov.moviedb.presentation.activities.tvshow

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShow
import org.bitbucket.draganbjedov.moviedb.databinding.ListItemBinding


class TvShowsListAdapter() : RecyclerView.Adapter<TvShowsListAdapter.ViewHolder>() {
    private val shows = ArrayList<TvShow>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun getItemCount(): Int = shows.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(shows[position])
    }

    fun setList(shows: List<TvShow>) {
        this.shows.clear()
        this.shows.addAll(shows)
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(show: TvShow) {
            binding.titleTextView.text = show.name
            binding.descriptionTextView.text = show.overview
            val posterURL = "https://image.tmdb.org/t/p/w500" + show.posterPath
            Glide.with(binding.imageView.context)
                .load(posterURL)
                .into(binding.imageView)

        }
    }
}

