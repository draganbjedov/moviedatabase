package org.bitbucket.draganbjedov.moviedb.data.sources.artist

import org.bitbucket.draganbjedov.moviedb.data.model.artist.Artist

interface ArtistLocalDataSource {
    suspend fun getArtists(): List<Artist>
    suspend fun saveArtists(artists: List<Artist>)
    suspend fun clear()
}
