package org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.impl

import org.bitbucket.draganbjedov.moviedb.BuildConfig
import org.bitbucket.draganbjedov.moviedb.data.api.TMDBService
import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShowList
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.TvShowRemoteDataSource
import retrofit2.Response
import javax.inject.Inject

class TvShowRemoteDataSourceImpl @Inject constructor(
    private val tmdbService: TMDBService
) : TvShowRemoteDataSource {

    override suspend fun getTvShows(): Response<TvShowList> = tmdbService.getPopularTvShows(BuildConfig.API_KEY)
}
