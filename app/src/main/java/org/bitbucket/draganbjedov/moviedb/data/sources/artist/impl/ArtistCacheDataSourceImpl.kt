package org.bitbucket.draganbjedov.moviedb.data.sources.artist.impl

import org.bitbucket.draganbjedov.moviedb.data.model.artist.Artist
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.ArtistCacheDataSource
import javax.inject.Inject

class ArtistCacheDataSourceImpl @Inject constructor(): ArtistCacheDataSource {

    private val artists =  ArrayList<Artist>()

    override suspend fun getArtists(): List<Artist> = artists

    override suspend fun saveArtists(artists: List<Artist>) {
        this.artists.clear()
        this.artists.addAll(artists)
    }
}
