package org.bitbucket.draganbjedov.moviedb.presentation.di.modules.core

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import org.bitbucket.draganbjedov.moviedb.data.db.LocalDatabase
import org.bitbucket.draganbjedov.moviedb.data.db.dao.ArtistDao
import org.bitbucket.draganbjedov.moviedb.data.db.dao.MovieDao
import org.bitbucket.draganbjedov.moviedb.data.db.dao.TvShowDao
import javax.inject.Singleton

@Module
class DatabaseModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideDatabase(): LocalDatabase = Room.databaseBuilder(
        context.applicationContext,
        LocalDatabase::class.java,
        "movies_tv_show.db"
    ).fallbackToDestructiveMigration()
        .build()

    @Provides
    @Singleton
    fun provideMovieDao(db: LocalDatabase): MovieDao = db.movieDao()

    @Provides
    @Singleton
    fun provideTvShowDao(db: LocalDatabase): TvShowDao = db.tvShowDao()

    @Provides
    @Singleton
    fun provideArtistDao(db: LocalDatabase): ArtistDao = db.artistDao()
}
