package org.bitbucket.draganbjedov.moviedb.data.repositories

import android.util.Log
import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShow
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.TvShowCacheDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.TvShowLocalDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.TvShowRemoteDataSource
import org.bitbucket.draganbjedov.moviedb.domain.repositories.TvShowRepository
import javax.inject.Inject

class TvShowRepositoryImpl @Inject constructor(
    private val tvShowRemoteDataSource: TvShowRemoteDataSource,
    private val tvShowsLocalDataSource: TvShowLocalDataSource,
    private val tvShowCacheDataSource: TvShowCacheDataSource
) : TvShowRepository {

    override suspend fun getTvShows(): List<TvShow> {
        return getTvShowsFromCache()
    }

    override suspend fun updateTvShows(): List<TvShow> {
        val shows = getMoviesFromRemote()
        tvShowsLocalDataSource.clear()
        tvShowsLocalDataSource.saveTvShows(shows)
        tvShowCacheDataSource.saveTvShows(shows)
        return shows
    }

    private suspend fun getMoviesFromRemote(): List<TvShow> {
        var shows: List<TvShow> = ArrayList()
        try {
            val response = tvShowRemoteDataSource.getTvShows()
            response.body()?.let {
                shows = it.tvShows
            }
        } catch (ex: Exception) {
            Log.e("MDTAG", "Failed to get shows from remote", ex)
        }

        return shows
    }

    private suspend fun getMoviesFromDatabase(): List<TvShow> {
        var shows = tvShowsLocalDataSource.getTvShows()
        if (shows.isEmpty()) {
            shows = getMoviesFromRemote()
            tvShowsLocalDataSource.saveTvShows(shows)
        }
        return shows
    }

    private suspend fun getTvShowsFromCache(): List<TvShow> {
        var shows = tvShowCacheDataSource.getTvShows()
        if (shows.isEmpty()) {
            shows = getMoviesFromDatabase()
            tvShowCacheDataSource.saveTvShows(shows)
        }
        return shows
    }
}
