package org.bitbucket.draganbjedov.moviedb.domain.usecases.movie

import org.bitbucket.draganbjedov.moviedb.data.model.movie.Movie
import org.bitbucket.draganbjedov.moviedb.domain.repositories.MovieRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UpdateMoviesUseCase @Inject constructor(private val movieRepository: MovieRepository) {

    suspend fun execute(): List<Movie> = movieRepository.updateMovies()
}
