package org.bitbucket.draganbjedov.moviedb.presentation.di.artist

import dagger.Module
import dagger.Provides
import org.bitbucket.draganbjedov.moviedb.domain.usecases.artist.GetArtistsUseCase
import org.bitbucket.draganbjedov.moviedb.domain.usecases.artist.UpdateArtistsUseCase
import org.bitbucket.draganbjedov.moviedb.presentation.activities.artist.ArtistsViewModelFactory

@Module
class ArtistModule {

    @Provides
    @ArtistScope
    fun provideArtistViewModelFactory(
        getArtistsUseCase: GetArtistsUseCase,
        updateArtistsUseCase: UpdateArtistsUseCase
    ): ArtistsViewModelFactory = ArtistsViewModelFactory(getArtistsUseCase, updateArtistsUseCase)
}
