package org.bitbucket.draganbjedov.moviedb.data.repositories

import android.util.Log
import org.bitbucket.draganbjedov.moviedb.data.model.movie.Movie
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.MovieCacheDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.MovieLocalDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.MovieRemoteDataSource
import org.bitbucket.draganbjedov.moviedb.domain.repositories.MovieRepository
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val movieRemoteDataSource: MovieRemoteDataSource,
    private val movieLocalDataSource: MovieLocalDataSource,
    private val movieCacheDataSource: MovieCacheDataSource
) : MovieRepository {

    override suspend fun getMovies(): List<Movie> {
        return getMoviesFromCache()
    }

    override suspend fun updateMovies(): List<Movie> {
        val movies = getMoviesFromRemote()
        movieLocalDataSource.clear()
        movieLocalDataSource.saveMovies(movies)
        movieCacheDataSource.saveMovies(movies)
        return movies
    }

    private suspend fun getMoviesFromRemote(): List<Movie> {
        var movies: List<Movie> = ArrayList()
        try {
            val response = movieRemoteDataSource.getMovies()
            response.body()?.let {
                movies = it.movies
            }
        } catch (ex: Exception) {
            Log.e("MDTAG", "Failed to get movies from remote", ex)
        }

        return movies
    }

    private suspend fun getMoviesFromDatabase(): List<Movie> {
        var movies = movieLocalDataSource.getMovies()
        if (movies.isEmpty()) {
            movies = getMoviesFromRemote()
            movieLocalDataSource.saveMovies(movies)
        }
        return movies
    }

    private suspend fun getMoviesFromCache(): List<Movie> {
        var movies = movieCacheDataSource.getMovies()
        if (movies.isEmpty()) {
            movies = getMoviesFromDatabase()
            movieCacheDataSource.saveMovies(movies)
        }
        return movies
    }
}
