package org.bitbucket.draganbjedov.moviedb.presentation.di

import dagger.Component
import org.bitbucket.draganbjedov.moviedb.presentation.di.artist.ArtistSubComponent
import org.bitbucket.draganbjedov.moviedb.presentation.di.modules.core.*
import org.bitbucket.draganbjedov.moviedb.presentation.di.movie.MovieSubComponent
import org.bitbucket.draganbjedov.moviedb.presentation.di.tvshow.TvShowSubComponent
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        RemoteDataSourceModule::class,
        LocalDataSourceModule::class,
        CacheDataSourceModule::class,
        RepositoryModule::class
    ]
)
interface AppComponent {
    fun movieSubComponent(): MovieSubComponent.Factory
    fun tvShowSubComponent(): TvShowSubComponent.Factory
    fun artistSubComponent(): ArtistSubComponent.Factory
}
