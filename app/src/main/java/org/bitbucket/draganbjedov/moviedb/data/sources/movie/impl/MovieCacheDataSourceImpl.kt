package org.bitbucket.draganbjedov.moviedb.data.sources.movie.impl

import org.bitbucket.draganbjedov.moviedb.data.model.movie.Movie
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.MovieCacheDataSource
import javax.inject.Inject

class MovieCacheDataSourceImpl @Inject constructor() : MovieCacheDataSource {

    private val movies = ArrayList<Movie>()

    override suspend fun getMovies(): List<Movie> = movies

    override suspend fun saveMovies(movies: List<Movie>) {
        this.movies.clear()
        this.movies.addAll(movies)
    }
}
