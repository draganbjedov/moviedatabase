package org.bitbucket.draganbjedov.moviedb.presentation.di

import org.bitbucket.draganbjedov.moviedb.presentation.di.artist.ArtistSubComponent
import org.bitbucket.draganbjedov.moviedb.presentation.di.movie.MovieSubComponent
import org.bitbucket.draganbjedov.moviedb.presentation.di.tvshow.TvShowSubComponent

interface Injector {
    fun createMovieSubComponent(): MovieSubComponent
    fun createTvShowSubComponent(): TvShowSubComponent
    fun createArtistSubComponent(): ArtistSubComponent
}
