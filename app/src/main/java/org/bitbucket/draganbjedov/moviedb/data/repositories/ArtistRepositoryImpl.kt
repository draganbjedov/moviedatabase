package org.bitbucket.draganbjedov.moviedb.data.repositories

import android.util.Log
import org.bitbucket.draganbjedov.moviedb.data.model.artist.Artist
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.ArtistCacheDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.ArtistLocalDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.ArtistRemoteDataSource
import org.bitbucket.draganbjedov.moviedb.domain.repositories.ArtistRepository
import javax.inject.Inject

class ArtistRepositoryImpl @Inject constructor(
    private val artistRemoteDataSource: ArtistRemoteDataSource,
    private val artistLocalDataSource: ArtistLocalDataSource,
    private val artistCacheDataSource: ArtistCacheDataSource
) : ArtistRepository {

    override suspend fun getArtists(): List<Artist> {
        return getArtistsFromCache()
    }

    override suspend fun updateArtists(): List<Artist> {
        val artists = getArtistsFromRemote()
        artistLocalDataSource.clear()
        artistLocalDataSource.saveArtists(artists)
        artistCacheDataSource.saveArtists(artists)
        return artists
    }

    private suspend fun getArtistsFromRemote(): List<Artist> {
        var artists: List<Artist> = ArrayList()
        try {
            val response = artistRemoteDataSource.getArtists()
            response.body()?.let {
                artists = it.artists
            }
        } catch (ex: Exception) {
            Log.e("MDTAG", "Failed to get artists from remote", ex)
        }

        return artists
    }

    private suspend fun getArtistsFromDatabase(): List<Artist> {
        var artists = artistLocalDataSource.getArtists()
        if (artists.isEmpty()) {
            artists = getArtistsFromRemote()
            artistLocalDataSource.saveArtists(artists)
        }
        return artists
    }

    private suspend fun getArtistsFromCache(): List<Artist> {
        var artists = artistCacheDataSource.getArtists()
        if (artists.isEmpty()) {
            artists = getArtistsFromDatabase()
            artistCacheDataSource.saveArtists(artists)
        }
        return artists
    }
}
