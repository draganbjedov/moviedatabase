package org.bitbucket.draganbjedov.moviedb.presentation.di.modules.core

import dagger.Module
import dagger.Provides
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.ArtistCacheDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.impl.ArtistCacheDataSourceImpl
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.MovieCacheDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.impl.MovieCacheDataSourceImpl
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.TvShowCacheDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.impl.TvShowCacheDataSourceImpl
import javax.inject.Singleton

@Module
class CacheDataSourceModule {

    @Provides
    @Singleton
    fun provideMovieCacheDataSource(dsImpl: MovieCacheDataSourceImpl): MovieCacheDataSource = dsImpl

    @Provides
    @Singleton
    fun provideTvShowCacheDataSource(dsImpl: TvShowCacheDataSourceImpl): TvShowCacheDataSource = dsImpl

    @Provides
    @Singleton
    fun provideArtistCacheDataSource(dsImpl: ArtistCacheDataSourceImpl): ArtistCacheDataSource = dsImpl
}
