package org.bitbucket.draganbjedov.moviedb.data.sources.movie

import org.bitbucket.draganbjedov.moviedb.data.model.movie.Movie

interface MovieCacheDataSource {
    suspend fun getMovies(): List<Movie>
    suspend fun saveMovies(movies: List<Movie>)
}
