package org.bitbucket.draganbjedov.moviedb.data.sources.movie

import org.bitbucket.draganbjedov.moviedb.data.model.movie.MovieList
import retrofit2.Response

interface MovieRemoteDataSource {
    suspend fun getMovies(): Response<MovieList>
}
