package org.bitbucket.draganbjedov.moviedb.presentation.activities.movie

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import org.bitbucket.draganbjedov.moviedb.R
import org.bitbucket.draganbjedov.moviedb.databinding.ActivityMoviesBinding
import org.bitbucket.draganbjedov.moviedb.presentation.di.Injector
import javax.inject.Inject

class MoviesActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMoviesBinding

    @Inject
    lateinit var viewModelFactory: MoviesViewModelFactory
    private lateinit var viewModel: MoviesViewModel

    private val adapter = MoviesListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMoviesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.activity_movies)

        (application as Injector).createMovieSubComponent().inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory).get(MoviesViewModel::class.java)

        binding.moviesList.apply {
            adapter = this@MoviesActivity.adapter
            layoutManager = LinearLayoutManager(this@MoviesActivity, LinearLayoutManager.VERTICAL, false)
        }

        binding.progressBar.visibility = View.VISIBLE
        viewModel.get().observe(this) {
            adapter.setList(it)
            binding.progressBar.visibility = View.GONE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_update) {
            binding.progressBar.visibility = View.VISIBLE
            viewModel.update().observe(this) {
                adapter.setList(it)
                binding.progressBar.visibility = View.GONE
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
