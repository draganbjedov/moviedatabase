package org.bitbucket.draganbjedov.moviedb.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import org.bitbucket.draganbjedov.moviedb.data.db.dao.ArtistDao
import org.bitbucket.draganbjedov.moviedb.data.db.dao.MovieDao
import org.bitbucket.draganbjedov.moviedb.data.db.dao.TvShowDao
import org.bitbucket.draganbjedov.moviedb.data.model.artist.Artist
import org.bitbucket.draganbjedov.moviedb.data.model.movie.Movie
import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShow

@Database(entities = [Artist::class, Movie::class, TvShow::class], version = 1, exportSchema = false)
abstract class LocalDatabase : RoomDatabase() {

    abstract fun movieDao(): MovieDao
    abstract fun tvShowDao(): TvShowDao
    abstract fun artistDao(): ArtistDao
}
