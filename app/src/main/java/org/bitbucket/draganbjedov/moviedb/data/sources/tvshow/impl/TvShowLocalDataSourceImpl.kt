package org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.impl

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.bitbucket.draganbjedov.moviedb.data.db.dao.TvShowDao
import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShow
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.TvShowLocalDataSource
import javax.inject.Inject

class TvShowLocalDataSourceImpl @Inject constructor(private val tvShowDao: TvShowDao) : TvShowLocalDataSource {

    override suspend fun getTvShows(): List<TvShow> = tvShowDao.getAll()

    override suspend fun saveTvShows(shows: List<TvShow>) {
        CoroutineScope(Dispatchers.IO).launch {
            tvShowDao.save(shows)
        }
    }

    override suspend fun clear() {
        CoroutineScope(Dispatchers.IO).launch {
            tvShowDao.deleteAll()
        }
    }
}
