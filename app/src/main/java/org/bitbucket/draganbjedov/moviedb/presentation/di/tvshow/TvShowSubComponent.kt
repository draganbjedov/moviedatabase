package org.bitbucket.draganbjedov.moviedb.presentation.di.tvshow

import dagger.Subcomponent
import org.bitbucket.draganbjedov.moviedb.presentation.activities.tvshow.TvShowsActivity

@TvShowScope
@Subcomponent(modules = [TvShowModule::class])
interface TvShowSubComponent {
    fun inject(activity: TvShowsActivity)

    @Subcomponent.Factory
    interface Factory {
        fun create(): TvShowSubComponent
    }
}
