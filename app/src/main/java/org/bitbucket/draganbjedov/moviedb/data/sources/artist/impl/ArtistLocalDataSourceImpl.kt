package org.bitbucket.draganbjedov.moviedb.data.sources.artist.impl

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.bitbucket.draganbjedov.moviedb.data.db.dao.ArtistDao
import org.bitbucket.draganbjedov.moviedb.data.model.artist.Artist
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.ArtistLocalDataSource
import javax.inject.Inject

class ArtistLocalDataSourceImpl @Inject constructor(private val artistDao: ArtistDao) : ArtistLocalDataSource {

    override suspend fun getArtists(): List<Artist> = artistDao.getAll()

    override suspend fun saveArtists(artists: List<Artist>) {
        CoroutineScope(Dispatchers.IO).launch {
            artistDao.save(artists)
        }
    }

    override suspend fun clear() {
        CoroutineScope(Dispatchers.IO).launch {
            artistDao.deleteAll()
        }
    }
}
