package org.bitbucket.draganbjedov.moviedb.presentation

import android.app.Application
import org.bitbucket.draganbjedov.moviedb.BuildConfig
import org.bitbucket.draganbjedov.moviedb.presentation.di.AppComponent
import org.bitbucket.draganbjedov.moviedb.presentation.di.DaggerAppComponent
import org.bitbucket.draganbjedov.moviedb.presentation.di.Injector
import org.bitbucket.draganbjedov.moviedb.presentation.di.artist.ArtistSubComponent
import org.bitbucket.draganbjedov.moviedb.presentation.di.modules.core.DatabaseModule
import org.bitbucket.draganbjedov.moviedb.presentation.di.modules.core.NetworkModule
import org.bitbucket.draganbjedov.moviedb.presentation.di.movie.MovieSubComponent
import org.bitbucket.draganbjedov.moviedb.presentation.di.tvshow.TvShowSubComponent

class App : Application(), Injector {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .networkModule(NetworkModule(BuildConfig.BASE_URL))
            .databaseModule(DatabaseModule(applicationContext))
            .build()
    }

    override fun createMovieSubComponent(): MovieSubComponent = appComponent.movieSubComponent().create()
    override fun createTvShowSubComponent(): TvShowSubComponent = appComponent.tvShowSubComponent().create()
    override fun createArtistSubComponent(): ArtistSubComponent = appComponent.artistSubComponent().create()
}
