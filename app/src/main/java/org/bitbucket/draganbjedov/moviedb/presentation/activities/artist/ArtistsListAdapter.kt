package org.bitbucket.draganbjedov.moviedb.presentation.activities.artist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import org.bitbucket.draganbjedov.moviedb.R
import org.bitbucket.draganbjedov.moviedb.data.model.artist.Artist
import org.bitbucket.draganbjedov.moviedb.databinding.ListItemBinding


class ArtistsListAdapter() : RecyclerView.Adapter<ArtistsListAdapter.ViewHolder>() {
    private val artists = ArrayList<Artist>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun getItemCount(): Int = artists.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(artists[position])
    }

    fun setList(artists: List<Artist>) {
        this.artists.clear()
        this.artists.addAll(artists)
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(artist: Artist) {
            binding.titleTextView.text = artist.name
            binding.descriptionTextView.text = artist.popularity.toString()
            val posterURL = "https://image.tmdb.org/t/p/w500" + artist.profilePath
            Glide.with(binding.imageView.context)
                .load(posterURL)
                .error(R.drawable.ic_artist)
                .into(binding.imageView)

        }
    }
}

