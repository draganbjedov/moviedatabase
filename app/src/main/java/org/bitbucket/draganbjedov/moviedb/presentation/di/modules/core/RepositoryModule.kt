package org.bitbucket.draganbjedov.moviedb.presentation.di.modules.core

import dagger.Module
import dagger.Provides
import org.bitbucket.draganbjedov.moviedb.data.repositories.ArtistRepositoryImpl
import org.bitbucket.draganbjedov.moviedb.data.repositories.MovieRepositoryImpl
import org.bitbucket.draganbjedov.moviedb.data.repositories.TvShowRepositoryImpl
import org.bitbucket.draganbjedov.moviedb.domain.repositories.ArtistRepository
import org.bitbucket.draganbjedov.moviedb.domain.repositories.MovieRepository
import org.bitbucket.draganbjedov.moviedb.domain.repositories.TvShowRepository
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesMovieRepository(repo: MovieRepositoryImpl): MovieRepository = repo

    @Provides
    @Singleton
    fun providesTvShowRepository(repo: TvShowRepositoryImpl): TvShowRepository = repo

    @Provides
    @Singleton
    fun providesArtistRepository(repo: ArtistRepositoryImpl): ArtistRepository = repo
}
