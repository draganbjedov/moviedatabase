package org.bitbucket.draganbjedov.moviedb.data.sources.tvshow

import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShow

interface TvShowLocalDataSource {
    suspend fun getTvShows(): List<TvShow>
    suspend fun saveTvShows(shows: List<TvShow>)
    suspend fun clear()
}
