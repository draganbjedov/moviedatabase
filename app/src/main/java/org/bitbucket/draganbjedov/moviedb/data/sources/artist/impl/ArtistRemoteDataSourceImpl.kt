package org.bitbucket.draganbjedov.moviedb.data.sources.artist.impl

import org.bitbucket.draganbjedov.moviedb.BuildConfig
import org.bitbucket.draganbjedov.moviedb.data.api.TMDBService
import org.bitbucket.draganbjedov.moviedb.data.model.artist.ArtistList
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.ArtistRemoteDataSource
import retrofit2.Response
import javax.inject.Inject

class ArtistRemoteDataSourceImpl @Inject constructor(
    private val tmdbService: TMDBService
) : ArtistRemoteDataSource {

    override suspend fun getArtists(): Response<ArtistList> = tmdbService.getPopularArtists(BuildConfig.API_KEY)
}
