package org.bitbucket.draganbjedov.moviedb.presentation.activities.tvshow

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import org.bitbucket.draganbjedov.moviedb.R
import org.bitbucket.draganbjedov.moviedb.databinding.ActivityTvShowsBinding
import org.bitbucket.draganbjedov.moviedb.presentation.di.Injector
import javax.inject.Inject

class TvShowsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTvShowsBinding

    @Inject
    lateinit var viewModelFactory: TvShowsViewModelFactory
    private lateinit var viewModel: TvShowsViewModel

    private val adapter = TvShowsListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTvShowsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.activity_tv_shows)

        (application as Injector).createTvShowSubComponent().inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory).get(TvShowsViewModel::class.java)

        binding.tvShowsList.apply {
            adapter = this@TvShowsActivity.adapter
            layoutManager = LinearLayoutManager(this@TvShowsActivity, LinearLayoutManager.VERTICAL, false)
        }

        binding.progressBar.visibility = View.VISIBLE
        viewModel.get().observe(this) {
            adapter.setList(it)
            binding.progressBar.visibility = View.GONE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_update) {
            binding.progressBar.visibility = View.VISIBLE
            viewModel.update().observe(this) {
                adapter.setList(it)
                binding.progressBar.visibility = View.GONE
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
