package org.bitbucket.draganbjedov.moviedb.domain.repositories

import org.bitbucket.draganbjedov.moviedb.data.model.movie.Movie

interface MovieRepository {

    suspend fun getMovies(): List<Movie>
    suspend fun updateMovies(): List<Movie>
}
