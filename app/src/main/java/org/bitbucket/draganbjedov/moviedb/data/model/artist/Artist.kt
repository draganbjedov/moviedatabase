package org.bitbucket.draganbjedov.moviedb.data.model.artist


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "artist")
data class Artist(
    @PrimaryKey
    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String,

    @SerializedName("popularity")
    val popularity: Double,

    @ColumnInfo(name = "profile_path", defaultValue = "")
    @SerializedName("profile_path")
    val profilePath: String
)
