package org.bitbucket.draganbjedov.moviedb.presentation.di.modules.core

import android.content.Context
import dagger.Module
import dagger.Provides
import org.bitbucket.draganbjedov.moviedb.presentation.di.artist.ArtistSubComponent
import org.bitbucket.draganbjedov.moviedb.presentation.di.movie.MovieSubComponent
import org.bitbucket.draganbjedov.moviedb.presentation.di.tvshow.TvShowSubComponent
import javax.inject.Singleton

@Module(
    subcomponents = [
        MovieSubComponent::class,
        TvShowSubComponent::class,
        ArtistSubComponent::class
    ]
)
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = context.applicationContext
}
