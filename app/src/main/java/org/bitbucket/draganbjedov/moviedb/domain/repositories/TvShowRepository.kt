package org.bitbucket.draganbjedov.moviedb.domain.repositories

import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShow

interface TvShowRepository {

    suspend fun getTvShows(): List<TvShow>
    suspend fun updateTvShows(): List<TvShow>
}
