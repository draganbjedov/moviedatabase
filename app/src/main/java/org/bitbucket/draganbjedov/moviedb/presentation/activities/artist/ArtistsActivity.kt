package org.bitbucket.draganbjedov.moviedb.presentation.activities.artist

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import org.bitbucket.draganbjedov.moviedb.R
import org.bitbucket.draganbjedov.moviedb.databinding.ActivityArtistsBinding
import org.bitbucket.draganbjedov.moviedb.presentation.di.Injector
import javax.inject.Inject

class ArtistsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityArtistsBinding

    @Inject
    lateinit var viewModelFactory: ArtistsViewModelFactory
    private lateinit var viewModel: ArtistsViewModel

    private val adapter = ArtistsListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArtistsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.activity_artists)

        (application as Injector).createArtistSubComponent().inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory).get(ArtistsViewModel::class.java)


        binding.artistsList.apply {
            adapter = this@ArtistsActivity.adapter
            layoutManager = LinearLayoutManager(this@ArtistsActivity, LinearLayoutManager.VERTICAL, false)
        }

        binding.progressBar.visibility = View.VISIBLE
        viewModel.get().observe(this) {
            adapter.setList(it)
            binding.progressBar.visibility = View.GONE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_update) {
            binding.progressBar.visibility = View.VISIBLE
            viewModel.update().observe(this) {
                adapter.setList(it)
                binding.progressBar.visibility = View.GONE
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
