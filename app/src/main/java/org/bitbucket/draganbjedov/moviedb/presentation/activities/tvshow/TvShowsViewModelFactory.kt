package org.bitbucket.draganbjedov.moviedb.presentation.activities.tvshow

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.bitbucket.draganbjedov.moviedb.domain.usecases.tvshow.GetTvShowsUseCase
import org.bitbucket.draganbjedov.moviedb.domain.usecases.tvshow.UpdateTvShowsUseCase

class TvShowsViewModelFactory(
    private val getTvShowsUseCase: GetTvShowsUseCase,
    private val updateTvShowsUseCase: UpdateTvShowsUseCase
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TvShowsViewModel::class.java))
            return TvShowsViewModel(getTvShowsUseCase, updateTvShowsUseCase) as T
        throw IllegalArgumentException()
    }
}
