package org.bitbucket.draganbjedov.moviedb.presentation.di.modules.core

import dagger.Module
import dagger.Provides
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.ArtistLocalDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.impl.ArtistLocalDataSourceImpl
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.MovieLocalDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.impl.MovieLocalDataSourceImpl
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.TvShowLocalDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.impl.TvShowLocalDataSourceImpl
import javax.inject.Singleton

@Module
class LocalDataSourceModule {

    @Provides
    @Singleton
    fun provideMovieLocalDataSource(dsImpl: MovieLocalDataSourceImpl): MovieLocalDataSource = dsImpl

    @Provides
    @Singleton
    fun provideTvShowLocalDataSource(dsImpl: TvShowLocalDataSourceImpl): TvShowLocalDataSource = dsImpl

    @Provides
    @Singleton
    fun provideArtistLocalDataSource(dsImpl: ArtistLocalDataSourceImpl): ArtistLocalDataSource = dsImpl
}
