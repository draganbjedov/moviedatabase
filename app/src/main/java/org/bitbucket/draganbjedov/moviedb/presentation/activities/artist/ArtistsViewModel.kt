package org.bitbucket.draganbjedov.moviedb.presentation.activities.artist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import org.bitbucket.draganbjedov.moviedb.domain.usecases.artist.GetArtistsUseCase
import org.bitbucket.draganbjedov.moviedb.domain.usecases.artist.UpdateArtistsUseCase

class ArtistsViewModel(
    private val getUseCase: GetArtistsUseCase,
    private val updateUseCase: UpdateArtistsUseCase
) : ViewModel() {

    fun get() = liveData {
        val artists = getUseCase.execute()
        emit(artists)
    }

    fun update() = liveData {
        val artists = updateUseCase.execute()
        emit(artists)
    }

}
