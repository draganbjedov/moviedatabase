package org.bitbucket.draganbjedov.moviedb.presentation.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.bitbucket.draganbjedov.moviedb.databinding.ActivityMainBinding
import org.bitbucket.draganbjedov.moviedb.presentation.activities.artist.ArtistsActivity
import org.bitbucket.draganbjedov.moviedb.presentation.activities.movie.MoviesActivity
import org.bitbucket.draganbjedov.moviedb.presentation.activities.tvshow.TvShowsActivity
import kotlin.reflect.KClass

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.moviesButton.setOnClickListener { startActivity(MoviesActivity::class) }
        binding.tvShowsButton.setOnClickListener { startActivity(TvShowsActivity::class) }
        binding.artistButton.setOnClickListener { startActivity(ArtistsActivity::class) }
    }

    private fun <T : Activity> startActivity(activityClass: KClass<T>) {
        val intent = Intent(this, activityClass.java)
        startActivity(intent)
    }
}
