package org.bitbucket.draganbjedov.moviedb.presentation.activities.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.bitbucket.draganbjedov.moviedb.domain.usecases.movie.GetMoviesUseCase
import org.bitbucket.draganbjedov.moviedb.domain.usecases.movie.UpdateMoviesUseCase

class MoviesViewModelFactory(
    private val getMoviesUseCase: GetMoviesUseCase,
    private val updateMoviesUseCase: UpdateMoviesUseCase
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MoviesViewModel::class.java))
            return MoviesViewModel(getMoviesUseCase, updateMoviesUseCase) as T
        throw IllegalArgumentException()
    }
}
