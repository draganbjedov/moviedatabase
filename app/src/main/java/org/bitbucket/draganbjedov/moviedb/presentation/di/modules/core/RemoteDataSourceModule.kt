package org.bitbucket.draganbjedov.moviedb.presentation.di.modules.core

import dagger.Module
import dagger.Provides
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.ArtistRemoteDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.artist.impl.ArtistRemoteDataSourceImpl
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.MovieRemoteDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.impl.MovieRemoteDataSourceImpl
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.TvShowRemoteDataSource
import org.bitbucket.draganbjedov.moviedb.data.sources.tvshow.impl.TvShowRemoteDataSourceImpl
import javax.inject.Singleton

@Module
class RemoteDataSourceModule {

    @Provides
    @Singleton
    fun provideMovieRemoteDataSource(dsImpl: MovieRemoteDataSourceImpl): MovieRemoteDataSource = dsImpl

    @Provides
    @Singleton
    fun provideTvShowRemoteDataSource(dsImpl: TvShowRemoteDataSourceImpl): TvShowRemoteDataSource = dsImpl

    @Provides
    @Singleton
    fun provideArtistRemoteDataSource(dsImpl: ArtistRemoteDataSourceImpl): ArtistRemoteDataSource = dsImpl
}
