package org.bitbucket.draganbjedov.moviedb.data.sources.movie.impl

import org.bitbucket.draganbjedov.moviedb.BuildConfig
import org.bitbucket.draganbjedov.moviedb.data.api.TMDBService
import org.bitbucket.draganbjedov.moviedb.data.model.movie.MovieList
import org.bitbucket.draganbjedov.moviedb.data.sources.movie.MovieRemoteDataSource
import retrofit2.Response
import javax.inject.Inject

class MovieRemoteDataSourceImpl @Inject constructor(
    private val tmdbService: TMDBService
) : MovieRemoteDataSource {

    override suspend fun getMovies(): Response<MovieList> = tmdbService.getPopularMovies(BuildConfig.API_KEY)
}
