package org.bitbucket.draganbjedov.moviedb.presentation.activities.tvshow

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import org.bitbucket.draganbjedov.moviedb.domain.usecases.tvshow.GetTvShowsUseCase
import org.bitbucket.draganbjedov.moviedb.domain.usecases.tvshow.UpdateTvShowsUseCase

class TvShowsViewModel(
    private val getUseCase: GetTvShowsUseCase,
    private val updateUseCase: UpdateTvShowsUseCase
) : ViewModel() {

    fun get() = liveData {
        val shows = getUseCase.execute()
        emit(shows)
    }

    fun update() = liveData {
        val shows = updateUseCase.execute()
        emit(shows)
    }

}
