package org.bitbucket.draganbjedov.moviedb.presentation.di.tvshow

import dagger.Module
import dagger.Provides
import org.bitbucket.draganbjedov.moviedb.domain.usecases.tvshow.GetTvShowsUseCase
import org.bitbucket.draganbjedov.moviedb.domain.usecases.tvshow.UpdateTvShowsUseCase
import org.bitbucket.draganbjedov.moviedb.presentation.activities.tvshow.TvShowsViewModelFactory

@Module
class TvShowModule {

    @Provides
    @TvShowScope
    fun provideTvShowViewModelFactory(
        getTvShowsUseCase: GetTvShowsUseCase,
        updateTvShowsUseCase: UpdateTvShowsUseCase
    ): TvShowsViewModelFactory = TvShowsViewModelFactory(getTvShowsUseCase, updateTvShowsUseCase)
}
