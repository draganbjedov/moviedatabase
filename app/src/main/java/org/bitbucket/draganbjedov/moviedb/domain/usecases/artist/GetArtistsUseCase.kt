package org.bitbucket.draganbjedov.moviedb.domain.usecases.artist

import org.bitbucket.draganbjedov.moviedb.data.model.artist.Artist
import org.bitbucket.draganbjedov.moviedb.domain.repositories.ArtistRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetArtistsUseCase @Inject constructor(private val artistRepository: ArtistRepository) {

    suspend fun execute(): List<Artist> = artistRepository.getArtists()
}
