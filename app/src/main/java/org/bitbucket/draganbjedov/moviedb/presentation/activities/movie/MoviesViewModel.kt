package org.bitbucket.draganbjedov.moviedb.presentation.activities.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import org.bitbucket.draganbjedov.moviedb.domain.usecases.movie.GetMoviesUseCase
import org.bitbucket.draganbjedov.moviedb.domain.usecases.movie.UpdateMoviesUseCase

class MoviesViewModel(
    private val getUseCase: GetMoviesUseCase,
    private val updateUseCase: UpdateMoviesUseCase
) : ViewModel() {

    fun get() = liveData {
        val movies = getUseCase.execute()
        emit(movies)
    }

    fun update() = liveData {
        val movies = updateUseCase.execute()
        emit(movies)
    }

}
