package org.bitbucket.draganbjedov.moviedb.presentation.di.artist

import dagger.Subcomponent
import org.bitbucket.draganbjedov.moviedb.presentation.activities.artist.ArtistsActivity

@ArtistScope
@Subcomponent(modules = [ArtistModule::class])
interface ArtistSubComponent {
    fun inject(activity: ArtistsActivity)

    @Subcomponent.Factory
    interface Factory {
        fun create(): ArtistSubComponent
    }
}
