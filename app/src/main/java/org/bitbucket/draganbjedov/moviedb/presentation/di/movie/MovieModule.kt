package org.bitbucket.draganbjedov.moviedb.presentation.di.movie

import dagger.Module
import dagger.Provides
import org.bitbucket.draganbjedov.moviedb.domain.usecases.movie.GetMoviesUseCase
import org.bitbucket.draganbjedov.moviedb.domain.usecases.movie.UpdateMoviesUseCase
import org.bitbucket.draganbjedov.moviedb.presentation.activities.movie.MoviesViewModelFactory

@Module
class MovieModule {

    @Provides
    @MovieScope
    fun provideMovieViewModelFactory(
        getMoviesUseCase: GetMoviesUseCase,
        updateMoviesUseCase: UpdateMoviesUseCase
    ): MoviesViewModelFactory = MoviesViewModelFactory(getMoviesUseCase, updateMoviesUseCase)
}
