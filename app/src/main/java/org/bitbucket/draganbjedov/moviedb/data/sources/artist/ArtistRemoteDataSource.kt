package org.bitbucket.draganbjedov.moviedb.data.sources.artist

import org.bitbucket.draganbjedov.moviedb.data.model.artist.ArtistList
import retrofit2.Response

interface ArtistRemoteDataSource {
    suspend fun getArtists(): Response<ArtistList>
}
