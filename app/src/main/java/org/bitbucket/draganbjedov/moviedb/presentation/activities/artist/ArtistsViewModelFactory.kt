package org.bitbucket.draganbjedov.moviedb.presentation.activities.artist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.bitbucket.draganbjedov.moviedb.domain.usecases.artist.GetArtistsUseCase
import org.bitbucket.draganbjedov.moviedb.domain.usecases.artist.UpdateArtistsUseCase

class ArtistsViewModelFactory(
    private val getArtistsUseCase: GetArtistsUseCase,
    private val updateArtistsUseCase: UpdateArtistsUseCase
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ArtistsViewModel::class.java))
            return ArtistsViewModel(getArtistsUseCase, updateArtistsUseCase) as T
        throw IllegalArgumentException()
    }
}
