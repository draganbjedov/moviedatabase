package org.bitbucket.draganbjedov.moviedb.data.sources.tvshow

import org.bitbucket.draganbjedov.moviedb.data.model.tvshow.TvShowList
import retrofit2.Response

interface TvShowRemoteDataSource {
    suspend fun getTvShows(): Response<TvShowList>
}
